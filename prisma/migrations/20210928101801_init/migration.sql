-- CreateTable
CREATE TABLE `Book` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `bookname` VARCHAR(255) NOT NULL,
    `author` VARCHAR(255) NOT NULL,

    UNIQUE INDEX `Book_bookname_key`(`bookname`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
