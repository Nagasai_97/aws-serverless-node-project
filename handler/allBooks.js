'use strict';
const utils = require('../utils/response')
const { PrismaClient } = require('@prisma/client')
const prisma = new PrismaClient()

exports.allBooks = async (event, context, callback) => {
  try {
    const books = await prisma.book.findMany({
      select:{
                id:true,
                bookname:true,
                author:true
            }
    })
    let res = utils.response(books)
    return res  
  } 
  catch (error) {
    let failureRes = utils.failureResponse(error)
    return failureRes
  }
}