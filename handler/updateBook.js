'use strict';
const utils = require('../utils/response')
const { PrismaClient } = require('@prisma/client')
const prisma = new PrismaClient()

exports.updateBook = async (event, context, callback) => { 
  try {
    let id = parseInt(event.pathParameters.id)
    const {bookname,author}= JSON.parse(event.body);
    const updateBook = await prisma.book.update({
        where:{
            id
        },
        data:{
            bookname:bookname,
            author:author
        }
    })
    let res = utils.response(updateBook)
    return res  
  } 
  catch (error) {
    let failureRes = utils.failureResponse(error)
    return failureRes
  }
}