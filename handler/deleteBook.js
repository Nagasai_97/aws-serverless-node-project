'use strict';
const utils = require('../utils/response')
const { PrismaClient } = require('@prisma/client')
const prisma = new PrismaClient()

exports.deleteBook = async (event, context, callback) => {
  try {
    let id =parseInt(event.pathParameters.id)
    const deleteBook = await prisma.book.delete({
        where:{
            id
        }
    })
    let res = utils.response(deleteBook)
    return res 
  } 
  catch (error) {
    let failureRes = utils.failureResponse(error)
    return failureRes
  }
}