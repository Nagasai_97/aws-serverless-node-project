'use strict';
const utils = require('../utils/response')
const { PrismaClient } = require('@prisma/client')
const prisma = new PrismaClient()

exports.post = async (event, context, callback) => {
  try {
    const {bookname,author}= JSON.parse(event.body);
    const createBook = await prisma.book.create({
        data:{
            bookname:bookname,
            author:author
        }
    });
    let res = utils.response(createBook)
    return res  
  } 
  catch (error) {
    let failureRes = utils.failureResponse(error)
    return failureRes
    
  }
}