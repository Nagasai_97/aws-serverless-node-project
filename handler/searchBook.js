'use strict';
const utils = require('../utils/response')

const { PrismaClient } = require('@prisma/client')
const prisma = new PrismaClient()

exports.searchBook = async (event, context, callback) => {
  
  try {
    const searchName = event.pathParameters.searchItem
    const searchBooks = await prisma.book.findMany({
        where:{
            OR:[
                {
                  bookname:{
                    startsWith: searchName,
                  }},
                {
                    author:{
                    startsWith: searchName,
                  },}
            ]
            
        }
    });
    let res = utils.response(searchBooks)
    return res  
  } 
  catch (error) {
    let failureRes = utils.failureResponse(error)
    return failureRes
  }
}