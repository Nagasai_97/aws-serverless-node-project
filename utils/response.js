exports.response=(data)=>{
    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true,
        'Access-Control-Allow-Methods': '*'
      },
      body: JSON.stringify(data)
    }
  }

  exports.failureResponse=(error)=>{
    console.log(error)
    return {
        statusCode: 500,
        headers: {
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Credentials': true,
          'Access-Control-Allow-Methods': '*'
        },
        body: JSON.stringify(error)
    }
  }